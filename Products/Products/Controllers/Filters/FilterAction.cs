﻿using Microsoft.AspNetCore.Mvc.Filters;
using Products.Services;
using Microsoft.AspNetCore.Mvc;

namespace Products.Controllers.Filters
{
    public class FilterAction : IActionFilter
    {
        private readonly IErrorService _errorService;
        private readonly ILogService _logService;

        public FilterAction(
            IErrorService errorService,
            ILogService logService)
        {
            _errorService = errorService;
            _logService = logService;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception != null)
            {
                _logService.LogException(context.Exception.ToString());
                return;
            }

            if (_errorService.HasErrors)
            {
                context.Result = new BadRequestObjectResult(_errorService);
                return;
            }
        }
    }
}
