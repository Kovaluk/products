﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Products.Services;

namespace Products.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SystemController : ControllerBase
    {
        private const string Authorization = "Authorization";

        private readonly ISystemService _systemService;

        public SystemController(ISystemService systemService)
        {
            _systemService = systemService;
        }

        [HttpGet]
        [Route("clear-security-codes")]
        public async Task<IActionResult> ClearSecurityCodesAsync()
        {
            await _systemService.ClearSecurityCodesAsync(GetKeyAuth());
            return Ok();
        }

        #region Private Methods

        private string GetKeyAuth()
        {
            return HttpContext.Request.Headers[Authorization];
        }

        #endregion
    }
}