﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Products.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace Products.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        #region User Methods

        [HttpPost]
        [Authorize(Roles = "User")]
        [Route("create")]
        public async Task<IActionResult> CreateProductAsync([FromForm] ProductInput productInput)
        {
            var vm = await _productService.CreateProductAsync(productInput);
            return Ok(vm);
        }

        [HttpGet]
        [Authorize(Roles = "User")]
        [Route("get-product-{productId}")]
        public async Task<IActionResult> GetProductAsync(int productId)
        {
            var vm = await _productService.GetProductAsync(productId);
            return Ok(vm);
        }

        [HttpPatch]
        [Authorize(Roles = "User")]
        [Route("update-product-{productId}")]
        public async Task<IActionResult> UpdateProductAsync(ProductInput productInput, int productId)
        {
            var vm = await _productService.UpdateProductAsync(productInput, productId);
            return Ok(vm);
        }

        [HttpDelete]
        [Authorize(Roles = "User")]
        [Route("delete-product-{productId}")]
        public async Task<IActionResult> DeleteProductAsync(int productId)
        {
            await _productService.DeleteProductAsync(productId);
            return Ok();
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        [Route("get-products")]
        public async Task<IActionResult> GetProductsAsync(ProductQuery productQuery)
        {
            var vm = await _productService.GetProductsAsync(productQuery);
            return Ok(vm);
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        [Route("add-product-image-{productId}")]
        public async Task<IActionResult> AddProductImageAsync(IFormFile formFile, int productId)
        {
            var vm = await _productService.AddProductImageAsync(formFile, productId);
            return Ok(vm);
        }

        [HttpDelete]
        [Authorize(Roles = "User")]
        [Route("delete-product/image-{imageId}")]
        public async Task<IActionResult> DeleteProductImageAsync(int imageId)
        {
            var vm = await _productService.DeleteProductImageAsync(imageId);
            return Ok(vm);
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        [Route("like-product-{productId}-{isLike}")]
        public async Task<IActionResult> LikeProductAsync(int productId, bool isLike)
        {
            var vm = await _productService.LikeProductAsync(productId, isLike);
            return Ok(vm);
        }

        #endregion

        #region Public Methods

        [HttpGet]
        [Route("get-product-types")]
        public async Task<IActionResult> GetProductTypesAsync()
        {
            var vm = await _productService.GetProductTypesAsync();
            return Ok(vm);
        }

        [HttpPost]
        [Route("get-products-public")]
        public async Task<IActionResult> GetProductsPublicAsync(ProductQuery productQuery)
        {
            var vm = await _productService.GetProductsPublicAsync(productQuery);
            return Ok(vm);
        }

        [HttpGet]
        [Route("get-product-public/{productId}")]
        public async Task<IActionResult> GetProductPublicAsync(int productId)
        {
            var vm = await _productService.GetProductPublicAsync(productId);
            return Ok(vm);
        }

        #endregion

        #region Admin Methods

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("get-products-by-admin")]
        public async Task<IActionResult> GetProductsByAdminAsync(ProductQuery productQuery)
        {
            var vm = await _productService.GetProductsByAdminAsync(productQuery);
            return Ok(vm);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("approve-product-by-admin-{productId}")]
        public async Task<IActionResult> ApproveProductByAdminAsync(ProductApproveInput productApproveInput, int productId)
        {
            await _productService.ApproveProductByAdminAsync(productApproveInput, productId);
            return Ok();
        }

        #endregion
    }
}