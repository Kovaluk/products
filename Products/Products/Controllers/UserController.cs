﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Products.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace Products.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        #region Public Methods

        [HttpGet]
        [Route("get-countries")]
        public async Task<IActionResult> GetCountriesAsync()
        {
            var vm = await _userService.GetCountriesAsync();
            return Ok(vm);
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateUserAsync(UserInput userInput)
        {
            await _userService.CreateUserAsync(userInput);
            return Ok();
        }

        [HttpPost]
        [Route("send-new-registration-code")]
        public async Task<IActionResult> SendNewRegistrationCodeAsync(EmailInput emailInput)
        {
            await _userService.SendNewRegistrationCodeAsync(emailInput);
            return Ok();
        }

        [HttpPost]
        [Route("user-activation")]
        public async Task<IActionResult> UserActivationAsync(UserActivation userActivation)
        {
            await _userService.UserActivationAsync(userActivation);
            return Ok();
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> LoginUserAsync(UserLoginInput userLoginInput)
        {
            var vm = await _userService.LoginUserAsync(userLoginInput);
            return Ok(vm);
        }

        [HttpPost]
        [Route("password-change-request")]
        public async Task<IActionResult> PasswordChangeRequestAsync(EmailInput emailInput)
        {
            await _userService.PasswordChangeRequestAsync(emailInput);
            return Ok();
        }

        [HttpPost]
        [Route("password-change-approve")]
        public async Task<IActionResult> PasswordChangeApproveAsync(UserPasswordChange userPasswordChange)
        {
            await _userService.PasswordChangeApproveAsync(userPasswordChange);
            return Ok();
        }

        [HttpGet]
        [Route("get-user-public-{userId}")]
        public async Task<IActionResult> GetUserPublicAsync(int userId)
        {
            var vm = await _userService.GetUserPublicAsync(userId);
            return Ok(vm);
        }

        #endregion

        #region User Methods

        [HttpPatch]
        [Authorize(Roles = "User")]
        [Route("update")]
        public async Task<IActionResult> UpdateUserAsync(UserUpdateInput userUpdateInput)
        {
            var vm = await _userService.UpdateUserAsync(userUpdateInput);
            return Ok(vm);
        }

        [HttpGet]
        [Authorize(Roles = "User")]
        [Route("get-user")]
        public async Task<IActionResult> GetUserAsync()
        {
            var vm = await _userService.GetUserAsync();
            return Ok(vm);
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        [Route("subscribe-user-{userId}")]
        public async Task<IActionResult> SubscribeUserAsync(int userId)
        {
            await _userService.SubscribeUserAsync(userId);
            return Ok();
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        [Route("unsubscribe-user-{userId}")]
        public async Task<IActionResult> UnsubscribeUserAsync(int userId)
        {
            await _userService.UnsubscribeUserAsync(userId);
            return Ok();
        }

        [HttpGet]
        [Authorize(Roles = "User")]
        [Route("get-subscriptions")]
        public async Task<IActionResult> GetSubscriptionsAsync()
        {
            var vm = await _userService.GetSubscriptionsAsync();
            return Ok(vm);
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        [Route("add-user-image")]
        public async Task<IActionResult> AddUserImageAsync(IFormFile formFile)
        {
            var vm = await _userService.AddUserImageAsync(formFile);
            return Ok(vm);
        }

        [HttpDelete]
        [Authorize(Roles = "User")]
        [Route("delete-user-image")]
        public async Task<IActionResult> DeleteUserImageAsync()
        {
            await _userService.DeleteUserImageAsync();
            return Ok();
        }

        #endregion

        #region Admin Methods

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("get-users-by-admin")]
        public async Task<IActionResult> GetUsersByAdminAsync(UserQuery userQuery)
        {
            var vm = await _userService.GetUsersByAdminAsync(userQuery);
            return Ok(vm);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("block-user-by-admin")]
        public async Task<IActionResult> BlockUserByAdminAsync(BlockUserInput blockUserInput)
        {
            await _userService.BlockUserByAdminAsync(blockUserInput);
            return Ok();
        }

        #endregion

    }
}
