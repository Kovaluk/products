﻿using Products.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace Products.Services
{
    public class UserContext : IUserContext
    {
        private readonly IHttpContextAccessor _contextAccessor;

        public UserContext(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        public bool IsAuthorized()
        {
            var user = _contextAccessor.HttpContext.User;
            return user != null;
        }

        public bool IsInRole(RoleType role)
        {
            var user = _contextAccessor.HttpContext.User;
            if (user != null)
            {
                return user.IsInRole(role.ToString("G"));
            }
            return false;
        }

        public int UserId
        {
            get
            {
                var user = _contextAccessor.HttpContext.User;
                if (user != null && !string.IsNullOrEmpty(user.Identity.Name))
                {
                    return int.Parse(user.Identity.Name);
                }
                return 0;
            }
        }
    }
}