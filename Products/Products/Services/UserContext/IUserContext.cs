﻿using Products.Models;
using System.Security.Claims;

namespace Products.Services
{
    public interface IUserContext
    {
        bool IsAuthorized();
        bool IsInRole(RoleType role);
        int UserId { get; }
    }
}
