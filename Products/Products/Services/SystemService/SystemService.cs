﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Products.DataContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Products.Models;

namespace Products.Services
{
    public class SystemService : ISystemService
    {
        private readonly Context _context;
        private readonly IConfiguration _configuration;

        public SystemService(
            Context context,
            IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task ClearSecurityCodesAsync(string keyAuth)
        {
            CheckAuth(keyAuth);

            var securityCodes = await _context.SecurityCodes
                .Where(x => x.Expired < DateTime.UtcNow)
                .ToListAsync();

            if (securityCodes.Any())
            {
                _context.SecurityCodes.RemoveRange(securityCodes);
                await _context.SaveChangesAsync();
            }
        }

        #region Private Methods

        private void CheckAuth(string keyAuth)
        {
            if (string.IsNullOrEmpty(keyAuth))
            {
                throw new Exception($"{nameof(keyAuth)} is null");
            }

            var keyAuthConfiguration = _configuration["Keys:SystemKeyAuth"];

            if (string.IsNullOrEmpty(keyAuthConfiguration))
            {
                throw new Exception($"{nameof(keyAuthConfiguration)} is null");
            }

            if (!string.Equals(keyAuthConfiguration, keyAuth))
            {
                throw new Exception($"{nameof(keyAuthConfiguration)} and {nameof(keyAuth)} are not equal");
            }
        }

        #endregion
    }
}
