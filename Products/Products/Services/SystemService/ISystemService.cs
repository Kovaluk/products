﻿using System.Threading.Tasks;

namespace Products.Services
{
    public interface ISystemService
    {
        Task ClearSecurityCodesAsync(string keyAuth);
    }
}
