﻿namespace Products.Services
{
    public enum EmailType
    {
        Registration = 1,
        PasswordChange = 2,
    }
}