﻿using System.Threading.Tasks;

namespace Products.Services
{
    public interface IEmailService
    {
        Task SendEmail(string email, string name, EmailType emailType, string code);
    }
}
