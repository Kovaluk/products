﻿using System;
using System.Threading.Tasks;
using Products.Services;
using System.Net;
using System.Net.Mail;
using Microsoft.Extensions.Configuration;

namespace Products.Services
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _configuration;

        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task SendEmail(string email, string name, EmailType emailType, string bodyText)
        {
            if (!CheckIsEnabled()) return;

            var websiteEmailName = _configuration["MailSettings:name"];
            var websiteEmail = _configuration["MailSettings:email"];
            var websiteEmailPassword = _configuration["MailSettings:password"];
            var smtpHost = _configuration["MailSettings:smtpHost"];
            var smtpPort = _configuration["MailSettings:smtpPort"];

            if (string.IsNullOrEmpty(websiteEmailName) || string.IsNullOrEmpty(websiteEmail) || string.IsNullOrEmpty(websiteEmailPassword))
            {
                throw new Exception($"Incorrect values in config: 'emailName' or 'email' or 'password'");
            }
            if (string.IsNullOrEmpty(smtpHost) || string.IsNullOrEmpty(smtpPort) || !int.TryParse(smtpPort, out int smtpPortValue))
            {
                throw new Exception($"Incorrect values in config: 'smtpHost' or 'smtpPort'");
            }

            var fromAddress = new MailAddress(websiteEmail, websiteEmailName);
            var toAddress = new MailAddress(email, name);
            var fromPassword = websiteEmailPassword;
            var subject = emailType.ToString("G");
            var body = bodyText;

            var smtp = new SmtpClient
            {
                Host = smtpHost,
                Port = smtpPortValue,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            };

            await smtp.SendMailAsync(message);
        }

        private bool CheckIsEnabled()
        {
            var isEnabled = _configuration["MailSettings:isEnabled"];

            if (string.IsNullOrEmpty(isEnabled))
            {
                throw new Exception($"Value '{nameof(isEnabled)}' not found");
            }

            if (!bool.TryParse(isEnabled, out bool isActive))
            {
                throw new Exception($"Value '{nameof(isEnabled)}' must have boolean type");
            }

            return isActive;
        }
    }
}