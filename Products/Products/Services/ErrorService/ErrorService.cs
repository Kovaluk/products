﻿using System.Collections.Generic;
using System.Linq;
using Products.Services;

namespace Products.Services
{
    public class ErrorService : IErrorService
    {
        public List<Error> Errors { get; set; }

        public ErrorService()
        {
            Errors = new List<Error>();
        }

        public void Add(Error error)
        {
            Errors.Add(error);
        }

        public void Add(ErrorCodes errorCode)
        {
            Errors.Add(new Error {
                Code = (int)errorCode,
                Description = errorCode.ToString("G"),
            });
        }

        public void Add(ErrorCodes errorCode, string value)
        {
            Errors.Add(new Error
            {
                Code = (int)errorCode,
                Description = errorCode.ToString("G"),
                Value = value,
            });
        }

        public bool HasErrors
        {
            get
            {
                return Errors.Any();
            }
        }
    }
}