﻿using Products.Services;

namespace Products.Services
{
    public interface IErrorService
    {
        void Add(Error error);
        void Add(ErrorCodes errorCode);
        void Add(ErrorCodes errorCode, string value);
        bool HasErrors { get; }
    }
}
