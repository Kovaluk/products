﻿namespace Products.Services
{
    public enum ErrorCodes
    {
        MODEL_IS_INVALID = 1,
        USER_EMAIL_ALREADY_EXISTS = 2,
        USER_NAME_ALREADY_EXISTS = 3,
        USER_NOT_FOUND = 4,
        USER_NOT_ACTIVE = 5,
        USER_BLOCKED_BY_ADMIN = 6,
        USER_SECURITY_CODE_INVALID = 7,
        DATA_NOT_FOUND = 8,
        FILES_NOT_FOUND = 9,
        FOLDER_NOT_FOUND = 10,
        DISALLOWED_WITH_YOUR_ITEM = 11,
        ITEM_ALREADY_EXISTS = 12,
        DISALLOWED_WITH_CURRENT_STATUS = 13,
        PLEASE_TRY_LATER = 14,
        FILE_TYPE_IS_INVALID = 15,
    }
}