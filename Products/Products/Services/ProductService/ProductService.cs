﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Products.DataContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Products.Models;
using Microsoft.AspNetCore.Http;

namespace Products.Services
{
    public class ProductService : IProductService
    {
        private readonly IErrorService _errorService;
        private readonly Context _context;
        private readonly IUserContext _userContext;
        private readonly IFileService _fileService;

        public ProductService(
            IErrorService errorService,
            Context context,
            IUserContext userContext,
            IFileService fileService)
        {
            _errorService = errorService;
            _context = context;
            _userContext = userContext;
            _fileService = fileService;
        }

        public async Task<ProductView> CreateProductAsync(ProductInput productInput)
        {
            if (string.IsNullOrEmpty(productInput.Name) || 
                string.IsNullOrEmpty(productInput.Description) || 
                !productInput.Mark.HasValue ||
                !productInput.ProductTypeId.HasValue)
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return null;
            }

            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == _userContext.UserId && x.IsActive);
            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return null;
            }

            var productType = await _context.ProductTypes.FirstOrDefaultAsync(x => x.Id == productInput.ProductTypeId.Value);
            if (productType == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return null;
            }

            bool isFileExists = productInput.FormFile != null;

            if (isFileExists)
            {
                bool isFileTypeValid = _fileService.IsFileTypeValid(productInput.FormFile, FileType.ProductImage);

                if (!isFileTypeValid)
                {
                    _errorService.Add(ErrorCodes.FILE_TYPE_IS_INVALID);
                    return null;
                }
            }

            var product = new Product
            {
                Name = productInput.Name,
                Description = productInput.Description,
                Mark = productInput.Mark.Value,
                Created = DateTime.UtcNow,
                User = user,
                ProductType = productType,
            };

            _context.Products.Add(product);

            if (isFileExists)
            {
                var fileNew = new File
                {
                    Name = productInput.FormFile.FileName,
                    Created = DateTime.UtcNow,
                    Type = FileType.ProductImage,
                    User = user,
                    Product = product,
                };

                _fileService.AddFile(productInput.FormFile, fileNew);

                _context.Files.Add(fileNew);
            }

            await _context.SaveChangesAsync();

            return product.ToView();
        }

        public async Task<ProductView> GetProductAsync(int productId)
        {
            if (productId == 0)
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return null;
            }

            var product = await _context.Products
                .Include(x => x.ProductType)
                .Include(x => x.Files)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == productId && x.UserId == _userContext.UserId);

            if (product == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return null;
            }

            return product.ToView();
        }

        public async Task<ProductView> UpdateProductAsync(ProductInput productInput, int productId)
        {
            if (productId == 0)
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return null;
            }

            var product = await _context.Products
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.User.Id == _userContext.UserId && x.Id == productId);

            if (product == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return null;
            }

            if (!string.IsNullOrEmpty(productInput.Name))
            {
                product.Name = productInput.Name;
            }
            if (!string.IsNullOrEmpty(productInput.Description))
            {
                product.Description = productInput.Description;
            }
            if (productInput.ProductTypeId.HasValue)
            {
                var productType = await _context.ProductTypes.FirstOrDefaultAsync(x => x.Id == productInput.ProductTypeId.Value);
                if (productType == null)
                {
                    _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                    return null;
                }
                product.ProductType = productType;
            }
            if (productInput.Mark.HasValue)
            {
                product.Mark = productInput.Mark.Value;
            }

            product.Status = ProductStatus.Pending;
            product.Updated = DateTime.UtcNow;

            await _context.SaveChangesAsync();

            return product.ToView();
        }

        public async Task DeleteProductAsync(int productId)
        {
            if (productId == 0)
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return;
            }

            var product = await _context.Products
                .Include(x => x.Files)
                .FirstOrDefaultAsync(x => x.UserId == _userContext.UserId && x.Id == productId);

            if (product == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return;
            }

            if (product.Files.Any())
            {
                foreach (var file in product.Files)
                {
                    _fileService.RemoveFile(file.Path);
                }
            }

            _context.Products.Remove(product);

            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<ProductView>> GetProductsAsync(ProductQuery productQuery)
        {
            IQueryable<Product> query = _context.Products
                .Include(x => x.ProductType)
                .Include(x => x.Files)
                .Include(x => x.ProductLikes)
                .Include(x => x.User)
                   .ThenInclude(u => u.Files)
                .Where(x => x.UserId == _userContext.UserId);

            var products = await FilterProductQuery(query, productQuery);

            return products.Select(x => x.ToView());
        }

        public async Task<ProductView> AddProductImageAsync(IFormFile formFile, int productId)
        {
            if (formFile == null)
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return null;
            }

            var user = await _context.Users
                .Include(x => x.Products)
                   .ThenInclude(x => x.Files)
                .FirstOrDefaultAsync(x => x.Id == _userContext.UserId);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return null;
            }

            var product = user.Products.FirstOrDefault(x => x.Id == productId);

            if (product == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return null;
            }

            var file = product.Files.FirstOrDefault();

            if (file != null)
            {
                _fileService.RemoveFile(file.Path);
                product.Files.Remove(file);
            }

            var fileNew = new File
            {
                Name = formFile.FileName,
                Created = DateTime.UtcNow,
                Type = FileType.ProductImage,
                User = user,
                Product = product,
            };

            _fileService.AddFile(formFile, fileNew);

            product.Files.Add(fileNew);

            product.Status = ProductStatus.Pending;
            product.Updated = DateTime.UtcNow;

            await _context.SaveChangesAsync();

            return product.ToView();
        }

        public async Task<ProductView> DeleteProductImageAsync(int imageId)
        {
            var file = await _context.Files
                .Include(x => x.Product)
                   .ThenInclude(p => p.ProductType)
                .FirstOrDefaultAsync(x => x.UserId == _userContext.UserId && x.Id == imageId);

            if (file == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return null;
            }

            var product = file.Product;

            product.Updated = DateTime.UtcNow;

            _fileService.RemoveFile(file.Path);
            _context.Files.Remove(file);

            await _context.SaveChangesAsync();

            return product.ToView();
        }

        public async Task<ProductPublicView> LikeProductAsync(int productId, bool isLike)
        {
            var product = await _context.Products
                .Include(x => x.ProductLikes)
                .FirstOrDefaultAsync(x => x.Id == productId);

            if (product == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return null;
            }

            var userId = _userContext.UserId;

            var like = product.ProductLikes.FirstOrDefault(x => x.UserId == userId);

            if (like != null)
            {
                product.ProductLikes.Remove(like);
            }

            if (like == null || like.IsLike != isLike)
            {
                product.ProductLikes.Add(new ProductLike
                {
                    UserId = userId,
                    IsLike = isLike,
                });
            }

            await _context.SaveChangesAsync();

            return product.ToPublicView(userId);
        }

        public async Task<IEnumerable<ProductTypeView>> GetProductTypesAsync()
        {
            var productTypes = await _context.ProductTypes
                .ToListAsync();

            if (productTypes == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return null;
            }

            return productTypes.Select(x => x.ToView());
        }

        public async Task<IEnumerable<ProductPublicView>> GetProductsPublicAsync(ProductQuery productQuery)
        {
            IQueryable<Product> query = _context.Products
                .Include(x => x.User)
                   .ThenInclude(u => u.Files)
                .Include(x => x.ProductType)
                .Include(x => x.ProductLikes)
                .Include(x => x.Files)
                .Where(x => x.Status == ProductStatus.Approved);

            var products = await FilterProductQuery(query, productQuery);

            return products.Select(x => x.ToPublicView(_userContext.UserId));
        }

        public async Task<ProductPublicView> GetProductPublicAsync(int productId)
        {
            var productPublic = await _context.Products
                .Include(x => x.ProductType)
                .Include(x => x.User)
                   .ThenInclude(u => u.Files)
                .Include(x => x.ProductLikes)
                .Include(x => x.Files)
                .FirstOrDefaultAsync(x => x.Id == productId && x.Status == ProductStatus.Approved);

            if (productPublic == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return null;
            }

            return productPublic.ToPublicView(_userContext.UserId);
        }

        public async Task<IEnumerable<ProductAdminView>> GetProductsByAdminAsync(ProductQuery productQuery)
        {
            IQueryable<Product> query = _context.Products
                .Include(x => x.User)
                   .ThenInclude(u => u.Files)
                .Include(x => x.ProductType)
                .Include(x => x.ProductLikes)
                .Include(x => x.Files);

            var products = await FilterProductQuery(query, productQuery);

            return products.Select(x => x.ToAdminView());
        }

        public async Task ApproveProductByAdminAsync(ProductApproveInput productApproveInput, int productId)
        {
            if (productId == 0 || productApproveInput == null)
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return;
            }

            var product = await _context.Products.FirstOrDefaultAsync(x => x.Id == productId);

            if (product == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return;
            }

            if (productApproveInput.Status.HasValue)
            {
                if (product.Status == productApproveInput.Status.Value)
                {
                    _errorService.Add(ErrorCodes.DISALLOWED_WITH_CURRENT_STATUS);
                    return;
                }

                product.Status = productApproveInput.Status.Value;
            }
            if (!string.IsNullOrEmpty(productApproveInput.AdminComment))
            {
                product.AdminComment = productApproveInput.AdminComment;
            }

            await _context.SaveChangesAsync();
        }

        #region Private Methods

        private async Task<List<Product>> FilterProductQuery(IQueryable<Product> query, ProductQuery productQuery)
        {
            if (productQuery.ProductTypeId.HasValue)
            {
                query = query.Where(x => x.ProductTypeId == productQuery.ProductTypeId.Value);
            }
            if (productQuery.UserId.HasValue)
            {
                query = query.Where(x => x.UserId == productQuery.UserId.Value);
            }
            if (!string.IsNullOrEmpty(productQuery.Name))
            {
                query = query.Where(x => x.Name.ToLower().Contains(productQuery.Name.ToLower()));
            }
            if (productQuery.DateFrom.HasValue)
            {
                query = query.Where(x => x.Created >= productQuery.DateFrom.Value);
            }
            if (productQuery.DateTo.HasValue)
            {
                query = query.Where(x => x.Created <= productQuery.DateTo.Value);
            }
            if (productQuery.Status.HasValue)
            {
                query = query.Where(x => x.Status == productQuery.Status.Value);
            }
            if (!string.IsNullOrEmpty(productQuery.UserName))
            {
                query = query.Where(x => x.User.Name.ToLower().Contains(productQuery.UserName.ToLower()));
            }

            if (productQuery.Skip.HasValue && productQuery.Take.HasValue)
            {
                query = query
                    .Skip(productQuery.Skip.Value)
                    .Take(productQuery.Take.Value);
            }

            if (productQuery.IsAscending)
            {
                query = query.OrderBy(x => x.Created);
            }
            else
            {
                query = query.OrderByDescending(x => x.Created);
            }

            return await query
                .AsNoTracking()
                .ToListAsync();
        }

        #endregion
    }
}
