﻿using System;
using System.Collections.Generic;
using Products.Models;
using System.Linq;

namespace Products.Services
{
    public class ProductPublicView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Mark { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public int UserId { get; set; }
        public ProductTypeView ProductType { get; set; }

        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public IEnumerable<FileView> Files { get; set; }

        public string UserName { get; set; }
        public string UserImage { get; set; }
        public bool? UserContextIsLike { get; set; }
    }

    public static class ProductPublicViewExtension
    {
        public static ProductPublicView ToPublicView(this Product product, int contextUserId = 0)
        {
            var productPublic = new ProductPublicView
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                Mark = product.Mark.ToString("G"),
                Created = product.Created,
                Updated = product.Updated,
                UserId = product.UserId,
                ProductType = product.ProductType?.ToView(),
                UserName = product.User?.Name,
            };

            if (product.Files != null && product.Files.Any())
            {
                productPublic.Files = product.Files.Select(x => x.ToView()).ToList();
            }

            if (product.ProductLikes != null && product.ProductLikes.Any())
            {
                productPublic.Likes = product.ProductLikes.Count(x => x.IsLike);
                productPublic.Dislikes = product.ProductLikes.Count(x => !x.IsLike);
            }

            if (contextUserId > 0)
            {
                var contextUserLike = product.ProductLikes.FirstOrDefault(x => x.UserId == contextUserId);

                if (contextUserLike != null)
                {
                    productPublic.UserContextIsLike = contextUserLike.IsLike;
                }
            }

            var productUserFiles = product?.User?.Files;

            if (productUserFiles != null && productUserFiles.Any(x => x.Type == FileType.UserImage))
            {
                productPublic.UserImage = productUserFiles.First(x => x.Type == FileType.UserImage).Path;
            }

            return productPublic;
        }
    }
}
