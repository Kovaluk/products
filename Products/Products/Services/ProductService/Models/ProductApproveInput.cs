﻿using Products.Models;

namespace Products.Services
{
    public class ProductApproveInput
    {
        public ProductStatus? Status { get; set; }
        public string AdminComment { get; set; }
    }
}
