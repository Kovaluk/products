﻿using System;
using Products.Models;

namespace Products.Services
{
    public class FileView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public DateTime Created { get; set; }
        public FileType Type { get; set; }
        public int UserId { get; set; }
        public int? ProductId { get; set; }
    }

    public static class FileViewExtension
    {
        public static FileView ToView(this File file)
        {
            return new FileView
            {
                Id = file.Id,
                Name = file.Name,
                Path = file.Path,
                Created = file.Created,
                Type = file.Type,
                UserId = file.UserId,
                ProductId = file.ProductId,
            };
        }
    }
}
