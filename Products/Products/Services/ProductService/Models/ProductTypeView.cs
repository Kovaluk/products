﻿using Products.Models;

namespace Products.Services
{
    public class ProductTypeView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public static class ProductTypeViewExtension
    {
        public static ProductTypeView ToView(this ProductType productType)
        {
            return new ProductTypeView
            {
                Id = productType.Id,
                Name = productType.Name,
                Description = productType.Description,
            };
        }
    }
}
