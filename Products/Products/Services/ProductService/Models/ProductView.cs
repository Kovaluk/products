﻿using System;
using System.Collections.Generic;
using Products.Models;
using System.Linq;

namespace Products.Services
{
    public class ProductView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Mark { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public int UserId { get; set; }
        public string Status { get; set; }
        public string AdminComment { get; set; }
        public ProductTypeView ProductType { get; set; }

        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public IEnumerable<FileView> Files { get; set; }

        public string UserImage { get; set; }
    }

    public static class ProductViewExtension
    {
        public static ProductView ToView(this Product product)
        {
            var productView = new ProductView
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                Mark = product.Mark.ToString("G"),
                Created = product.Created,
                Updated = product.Updated,
                Status = product.Status.ToString("G"),
                AdminComment = product.AdminComment,
                ProductType = product.ProductType?.ToView(),
                UserId = product.UserId
            };

            if (product.Files != null && product.Files.Any())
            {
                productView.Files = product.Files.Select(x => x.ToView()).ToList();
            }

            if (product.ProductLikes != null && product.ProductLikes.Any())
            {
                productView.Likes = product.ProductLikes.Count(x => x.IsLike);
                productView.Dislikes = product.ProductLikes.Count(x => !x.IsLike);
            }

            var productUserFiles = product?.User?.Files;

            if (productUserFiles != null && productUserFiles.Any(x => x.Type == FileType.UserImage))
            {
                productView.UserImage = productUserFiles.First(x => x.Type == FileType.UserImage).Path;
            }

            return productView;
        }
    }
}
