﻿using System;
using Products.ModelsCommon;
using Products.Models;

namespace Products.Services
{
    public class ProductQuery : BaseQuery
    {
        public int? UserId { get; set; }
        public string Name { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public ProductStatus? Status { get; set; }
        public string UserName { get; set; }
        public int? ProductTypeId { get; set; }
    }
}
