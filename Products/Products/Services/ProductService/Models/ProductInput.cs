﻿using Microsoft.AspNetCore.Http;
using Products.Models;

namespace Products.Services
{
    public class ProductInput
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ProductTypeId { get; set; }
        public ProductMark? Mark { get; set; }
        public IFormFile FormFile { get; set; }
    }
}
