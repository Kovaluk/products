﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Products.Services;
using Microsoft.AspNetCore.Http;
using System.IO;
namespace Products.Services
{
    public interface IProductService
    {
        Task<ProductView> CreateProductAsync(ProductInput productInput);
        Task<ProductView> GetProductAsync(int productId);
        Task<ProductView> UpdateProductAsync(ProductInput productInput, int productId);
        Task DeleteProductAsync(int productId);
        Task<IEnumerable<ProductView>> GetProductsAsync(ProductQuery productQuery);
        Task<ProductView> AddProductImageAsync(IFormFile formFile, int productId);
        Task<ProductView> DeleteProductImageAsync(int imageId);
        Task<ProductPublicView> LikeProductAsync(int productId, bool isLike);

        Task<IEnumerable<ProductTypeView>> GetProductTypesAsync();
        Task<IEnumerable<ProductPublicView>> GetProductsPublicAsync(ProductQuery productQuery);
        Task<ProductPublicView> GetProductPublicAsync(int productId);

        Task<IEnumerable<ProductAdminView>> GetProductsByAdminAsync(ProductQuery productQuery);
        Task ApproveProductByAdminAsync(ProductApproveInput productApproveInput, int productId);
    }
}
