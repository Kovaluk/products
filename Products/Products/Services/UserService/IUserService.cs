﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Products.Services
{
    public interface IUserService
    {
        Task<IEnumerable<CountryView>> GetCountriesAsync();
        Task CreateUserAsync(UserInput userInput);
        Task SendNewRegistrationCodeAsync(EmailInput emailInput);
        Task UserActivationAsync(UserActivation userActivation);
        Task<UserView> LoginUserAsync(UserLoginInput userLoginInput);
        Task PasswordChangeRequestAsync(EmailInput emailInput);
        Task PasswordChangeApproveAsync(UserPasswordChange userPasswordChange);
        Task<UserPublicView> GetUserPublicAsync(int userId);

        Task<UserView> UpdateUserAsync(UserUpdateInput userUpdateInput);
        Task<UserView> GetUserAsync();
        Task SubscribeUserAsync(int userId);
        Task UnsubscribeUserAsync(int userId);
        Task<IEnumerable<UserSubscriptionPublicView>> GetSubscriptionsAsync();
        Task<UserView> AddUserImageAsync(IFormFile formFile);
        Task DeleteUserImageAsync();

        Task<IEnumerable<UserAdminView>> GetUsersByAdminAsync(UserQuery userQuery);
        Task BlockUserByAdminAsync(BlockUserInput blockUserInput);
    }
}
