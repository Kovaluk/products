﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using Products.DataContext;
using Products.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using DIR = System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;

namespace Products.Services
{
    public class UserService : IUserService
    {
        private readonly IErrorService _errorService;
        private readonly IJWTService _jWTService;
        private readonly Context _context;
        private readonly IEmailService _emailService;
        private readonly IUserContext _userContext;
        private readonly IConfiguration _configuration;
        private readonly IFileService _fileService;

        public UserService(
            IErrorService errorService,
            IJWTService jWTService,
            Context context,
            IEmailService emailService,
            IUserContext userContext,
            IConfiguration configuration,
            IFileService fileService)
        {
            _errorService = errorService;
            _jWTService = jWTService;
            _context = context;
            _emailService = emailService;
            _userContext = userContext;
            _configuration = configuration;
            _fileService = fileService;
        }

        #region Public methods

        public async Task<IEnumerable<CountryView>> GetCountriesAsync()
        {
            var countries = await _context.Countries
                .AsNoTracking()
                .ToListAsync();
            return countries.Select(x => x.ToView());
        }

        public async Task CreateUserAsync(UserInput userInput)
        {
            if (string.IsNullOrEmpty(userInput.Email) || string.IsNullOrEmpty(userInput.Password))
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return;
            }

            var userExist = await _context.Users.AsNoTracking().AnyAsync(x => x.Email == userInput.Email);

            if (userExist)
            {
                _errorService.Add(ErrorCodes.USER_EMAIL_ALREADY_EXISTS);
                return;
            }

            var userNameExist = await _context.Users.AsNoTracking().AnyAsync(x => x.Name == userInput.Name);

            if (userNameExist)
            {
                _errorService.Add(ErrorCodes.USER_NAME_ALREADY_EXISTS);
                return;
            }

            var user = new User
            {
                Email = userInput.Email,
                Name = userInput.Name,
                Password = CreateHash(userInput.Password),
                Created = DateTime.UtcNow,
            };

            var userRole = await _context.Roles.FirstOrDefaultAsync(x => x.RoleType == RoleType.User);

            var securityCode = new SecurityCode
            {
                Code = GetRandomCode(),
                Created = DateTime.UtcNow,
                Expired = DateTime.UtcNow.AddDays(1),
                Type = SecurityCodeType.Registration,
            };

            user.RoleUsers.Add(new RoleUser
            {
                Role = userRole,
            });

            user.SecurityCodes.Add(securityCode);

            _context.Users.Add(user);

            await _emailService.SendEmail(user.Email, user.Name, EmailType.Registration, securityCode.Code);

            await _context.SaveChangesAsync();
        }

        public async Task SendNewRegistrationCodeAsync(EmailInput emailInput)
        {
            if (string.IsNullOrEmpty(emailInput.Email))
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return;
            }

            var user = await _context.Users
                .Include(x => x.SecurityCodes)
                .FirstOrDefaultAsync(x => x.Email == emailInput.Email && !x.IsActive);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return;
            }
            if (user.IsBlocked)
            {
                _errorService.Add(ErrorCodes.USER_BLOCKED_BY_ADMIN);
                return;
            }

            var userSecurityCodesRegistration = user.SecurityCodes.Where(x => x.Type == SecurityCodeType.Registration);

            if (userSecurityCodesRegistration.Any())
            {
                var userSecurityCodeRegistration = userSecurityCodesRegistration.FirstOrDefault();

                int emailSecurityCodeDelayMinutes = GetEmailSecurityCodeDelayMinutes(userSecurityCodeRegistration.Created);
                if (emailSecurityCodeDelayMinutes != 0)
                {
                    _errorService.Add(ErrorCodes.PLEASE_TRY_LATER, $"{emailSecurityCodeDelayMinutes} min");
                    return;
                }

                _context.SecurityCodes.RemoveRange(userSecurityCodesRegistration);
            }

            var securityCode = new SecurityCode
            {
                Code = GetRandomCode(),
                Created = DateTime.UtcNow,
                Expired = DateTime.UtcNow.AddDays(1),
                Type = SecurityCodeType.Registration,
                User = user,
            };

            _context.SecurityCodes.Add(securityCode);

            await _emailService.SendEmail(user.Email, user.Name, EmailType.Registration, securityCode.Code);

            await _context.SaveChangesAsync();
        }

        public async Task UserActivationAsync(UserActivation userActivation)
        {
            if (string.IsNullOrEmpty(userActivation.Email) || string.IsNullOrEmpty(userActivation.Code))
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return;
            }

            var user = await _context.Users
                .Include(x => x.SecurityCodes)
                .FirstOrDefaultAsync(x => x.Email == userActivation.Email && !x.IsActive);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return;
            }

            var securityCode = user.SecurityCodes.FirstOrDefault(x =>
               x.Type == SecurityCodeType.Registration &&
               x.Code == userActivation.Code &&
               x.Expired > DateTime.UtcNow);

            if (securityCode == null)
            {
                _errorService.Add(ErrorCodes.USER_SECURITY_CODE_INVALID);
                return;
            }

            user.IsActive = true;
            _context.SecurityCodes.Remove(securityCode);
            await _context.SaveChangesAsync();
        }

        public async Task<UserView> LoginUserAsync(UserLoginInput userLoginInput)
        {
            if (string.IsNullOrEmpty(userLoginInput.Email) || string.IsNullOrEmpty(userLoginInput.Password))
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return null;
            }

            var userPasswordHashed = CreateHash(userLoginInput.Password);

            var user = await _context.Users
                .Include(x => x.RoleUsers)
                   .ThenInclude(x => x.Role)
                .Include(x => x.SecurityCodes)
                .Include(x => x.Files)
                .FirstOrDefaultAsync(x => x.Email == userLoginInput.Email && x.Password == userPasswordHashed);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return null;
            }
            if (user.IsBlocked)
            {
                _errorService.Add(ErrorCodes.USER_BLOCKED_BY_ADMIN);
                return null;
            }
            if (!user.IsActive)
            {
                _errorService.Add(ErrorCodes.USER_NOT_ACTIVE);
                return null;
            }

            user.LoginDate = DateTime.UtcNow;
            await _context.SaveChangesAsync();

            var token = GetTokenView(user);

            return user.ToView(token);
        }

        public async Task PasswordChangeRequestAsync(EmailInput emailInput)
        {
            if (string.IsNullOrEmpty(emailInput.Email))
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return;
            }

            var user = await _context.Users
                .Include(x => x.SecurityCodes)
                .FirstOrDefaultAsync(x => x.Email == emailInput.Email);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return;
            }
            if (user.IsBlocked)
            {
                _errorService.Add(ErrorCodes.USER_BLOCKED_BY_ADMIN);
                return;
            }

            var userCodesPasswordChange = user.SecurityCodes.Where(x => x.Type == SecurityCodeType.PasswordChange);

            if (userCodesPasswordChange.Any())
            {
                var userCodePasswordChange = userCodesPasswordChange.FirstOrDefault();

                int emailSentLastDelayMinutes = GetEmailSecurityCodeDelayMinutes(userCodePasswordChange.Created);
                if (emailSentLastDelayMinutes != 0)
                {
                    _errorService.Add(ErrorCodes.PLEASE_TRY_LATER, $"{emailSentLastDelayMinutes} min");
                    return;
                }

                _context.SecurityCodes.RemoveRange(userCodesPasswordChange);
            }

            var securityCode = new SecurityCode
            {
                Code = GetRandomCode(),
                Created = DateTime.UtcNow,
                Expired = DateTime.UtcNow.AddDays(1),
                Type = SecurityCodeType.PasswordChange,
            };

            user.SecurityCodes.Add(securityCode);

            await _emailService.SendEmail(user.Email, user.Name, EmailType.PasswordChange, securityCode.Code);

            await _context.SaveChangesAsync();
        }

        public async Task PasswordChangeApproveAsync(UserPasswordChange userPasswordChange)
        {
            if (string.IsNullOrEmpty(userPasswordChange.Email) || string.IsNullOrEmpty(userPasswordChange.Password)
                || string.IsNullOrEmpty(userPasswordChange.Code))
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return;
            }

            var user = await _context.Users
                .Include(x => x.SecurityCodes)
                .FirstOrDefaultAsync(x => x.Email == userPasswordChange.Email);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return;
            }

            var securityCode = user.SecurityCodes.FirstOrDefault(x => x.Code == userPasswordChange.Code
               && x.Expired > DateTime.UtcNow
               && x.Type == SecurityCodeType.PasswordChange);

            if (securityCode != null)
            {
                user.Password = CreateHash(userPasswordChange.Password);
                _context.SecurityCodes.Remove(securityCode);
                await _context.SaveChangesAsync();
            }
            else
            {
                _errorService.Add(ErrorCodes.USER_SECURITY_CODE_INVALID);
                return;
            }
        }

        public async Task<UserPublicView> GetUserPublicAsync(int userId)
        {
            var user = await _context.Users
                .Include(x => x.Country)
                .Include(x => x.Subscribers)
                .Include(x => x.Products)
                .Include(x => x.Files)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return null;
            }

            return user.ToPublicView(_userContext.UserId);
        }

        public async Task<UserView> UpdateUserAsync(UserUpdateInput userUpdateInput)
        {
            var user = await _context.Users
                .Include(x => x.Country)
                .Include(x => x.RoleUsers)
                   .ThenInclude(x => x.Role)
                .FirstOrDefaultAsync(x => x.Id == _userContext.UserId);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return null;
            }

            if (userUpdateInput.Age.HasValue)
            {
                user.Age = userUpdateInput.Age.Value;
            }
            if (userUpdateInput.Gender.HasValue)
            {
                user.Gender = userUpdateInput.Gender.Value;
            }
            if (userUpdateInput.IsMarried.HasValue)
            {
                user.IsMarried = userUpdateInput.IsMarried.Value;
            }
            if (!string.IsNullOrEmpty(userUpdateInput.Description))
            {
                user.Description = userUpdateInput.Description;
            }

            if (userUpdateInput.CountryId.HasValue)
            {
                var country = await _context.Countries.FirstOrDefaultAsync(x => x.Id == userUpdateInput.CountryId.Value);

                if (country != null)
                {
                    user.Country = country;
                }
            }

            await _context.SaveChangesAsync();

            return user.ToView();
        }

        public async Task<UserView> GetUserAsync()
        {
            var user = await _context.Users
                .Include(x => x.RoleUsers)
                   .ThenInclude(x => x.Role)
                .Include(x => x.Country)
                .Include(x => x.Subscribers)
                   .ThenInclude(x => x.Subscriber)
                .Include(x => x.Subscriptions)
                   .ThenInclude(x => x.User)
                .Include(x => x.Products)
                .Include(x => x.Files)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == _userContext.UserId);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return null;
            }

            return user.ToView();
        }

        public async Task SubscribeUserAsync(int userId)
        {
            int currentUserId = _userContext.UserId;

            if (userId == 0 || userId == currentUserId)
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return;
            }

            var userSub = await _context.Users
                .Include(x => x.Subscribers)
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (userSub == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return;
            }

            if (userSub.Subscribers.Any(x => x.SubscriberId == currentUserId))
            {
                _errorService.Add(ErrorCodes.ITEM_ALREADY_EXISTS);
                return;
            }

            var subscription = new Subscription
            {
                SubscriberId = currentUserId,
                UserId = userId
            };

            _context.Subscriptions.Add(subscription);
            await _context.SaveChangesAsync();
        }

        public async Task UnsubscribeUserAsync(int userId)
        {
            if (userId == 0)
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return;
            }

            var subscription = await _context.Subscriptions
                .FirstOrDefaultAsync(x => x.SubscriberId == _userContext.UserId && x.UserId == userId);

            if (subscription == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return;
            }

            _context.Subscriptions.Remove(subscription);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserSubscriptionPublicView>> GetSubscriptionsAsync()
        {
            var subscriptions = await _context.Subscriptions
                .Include(x => x.User)
                   .ThenInclude(u => u.Products)
                .Where(x => x.SubscriberId == _userContext.UserId)
                .AsNoTracking()
                .ToListAsync();

            if (subscriptions == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return null;
            }

            var result = subscriptions.Select(x => x.User.ToUserSubscriptionPublicViewView());
            return result.OrderByDescending(x => x.LastUpdate);
        }

        public async Task<UserView> AddUserImageAsync(IFormFile formFile)
        {
            bool isFileTypeValid = _fileService.IsFileTypeValid(formFile, FileType.UserImage);

            if (!isFileTypeValid)
            {
                _errorService.Add(ErrorCodes.FILE_TYPE_IS_INVALID);
                return null;
            }

            var user = await _context.Users
                .Include(x => x.Files)
                .FirstOrDefaultAsync(x => x.Id == _userContext.UserId);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return null;
            }

            var userImage = user.Files.FirstOrDefault(x => x.Type == FileType.UserImage);

            if (userImage != null)
            {
                _fileService.RemoveFile(userImage.Path);
                user.Files.Remove(userImage);
            }

            var userImageNew = new File
            {
                Name = formFile.FileName,
                Created = DateTime.UtcNow,
                Type = FileType.UserImage,
                User = user,
            };

            _fileService.AddFile(formFile, userImageNew);

            user.Files.Add(userImageNew);

            await _context.SaveChangesAsync();

            return user.ToView();
        }

        public async Task DeleteUserImageAsync()
        {
            var user = await _context.Users
                .Include(x => x.Files)
                .FirstOrDefaultAsync(x => x.Id == _userContext.UserId);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.USER_NOT_FOUND);
                return;
            }

            var userImage = user.Files.FirstOrDefault(x => x.Type == FileType.UserImage);

            if (userImage == null)
            {
                _errorService.Add(ErrorCodes.FILES_NOT_FOUND);
                return;
            }

            _fileService.RemoveFile(userImage.Path);
            user.Files.Remove(userImage);

            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserAdminView>> GetUsersByAdminAsync(UserQuery userQuery)
        {
            IQueryable<User> usersQuerable = _context.Users;

            if (userQuery != null)
            {
                if (userQuery.UserId.HasValue)
                {
                    usersQuerable = usersQuerable.Where(x => x.Id == userQuery.UserId.Value);
                }
                if (!string.IsNullOrEmpty(userQuery.Name))
                {
                    usersQuerable = usersQuerable.Where(x => x.Name == userQuery.Name);
                }
                if (!string.IsNullOrEmpty(userQuery.Email))
                {
                    usersQuerable = usersQuerable.Where(x => x.Email == userQuery.Email);
                }
                if (userQuery.IsBlocked.HasValue)
                {
                    usersQuerable = usersQuerable.Where(x => x.IsBlocked == userQuery.IsBlocked.Value);
                }
            }

            var users = await usersQuerable
                .Include(x => x.Files)
                .AsNoTracking()
                .ToListAsync();

            return users.Select(x => x.ToAdminView());
        }

        public async Task BlockUserByAdminAsync(BlockUserInput blockUserInput)
        {
            if (blockUserInput == null || blockUserInput.UserId == 0 || !blockUserInput.IsBlocked.HasValue)
            {
                _errorService.Add(ErrorCodes.MODEL_IS_INVALID);
                return;
            }

            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == blockUserInput.UserId);

            if (user == null)
            {
                _errorService.Add(ErrorCodes.DATA_NOT_FOUND);
                return;
            }

            user.IsBlocked = blockUserInput.IsBlocked.Value;

            await _context.SaveChangesAsync();
        }

        #endregion

        #region Private Methods

        private string CreateHash(string inputString)
        {
            string GetStringFromHash(byte[] hashValue)
            {
                StringBuilder result = new StringBuilder();
                for (int i = 0; i < hashValue.Length; i++)
                {
                    result.Append(hashValue[i].ToString("X2"));
                }
                return result.ToString();
            }

            var sha512 = SHA512.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha512.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        private string GetTokenView(User user)
        {
            return _jWTService.GetToken(user);
        }

        private string GetRandomCode()
        {
            var random = new Random();
            var number = random.Next(10000, 99999);
            return number.ToString();
        }

        private int GetEmailSecurityCodeDelayMinutes(DateTime securityCodeCreated)
        {
            var emailSecurityCodeDelayMinutes = _configuration["Settings:EmailSecurityCodeDelayMinutes"];
            if (!int.TryParse(emailSecurityCodeDelayMinutes, out int emailSecurityCodeDelayMinutesValue))
            {
                throw new Exception($"Incorrect value in config: {nameof(emailSecurityCodeDelayMinutes)}");
            }

            DateTime securityCodeCreatedWithDelay = securityCodeCreated.AddMinutes(emailSecurityCodeDelayMinutesValue);

            int dateDiffMinutes = (securityCodeCreatedWithDelay - DateTime.UtcNow).Minutes;

            if (dateDiffMinutes > 0)
            {
                return dateDiffMinutes;
            }

            return 0;
        }

        #endregion
    }
}
