﻿using System;
using System.Linq;
using System.Collections.Generic;
using Products.Models;

namespace Products.Services
{
    public class UserView
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public DateTime Created { get; set; }
        public DateTime? LoginDate { get; set; }
        public string Name { get; set; }
        public int? Age { get; set; }
        public string Gender { get; set; }
        public string IsMarried { get; set; }
        public string Description { get; set; }
        public string UserImage { get; set; }
        public string Token { get; set; }
        public List<string> Roles { get; set; }
        public CountryView Country { get; set; }
        public IEnumerable<UserSub> Subscriptions { get; set; }
        public IEnumerable<UserSub> Subscribers { get; set; }
        public int Products { get; set; }
    }

    public static class UserViewExtension
    {
        public static UserView ToView(this User user, string token = null)
        {
            var userView = new UserView
            {
                Id = user.Id,
                Email = user.Email,
                Created = user.Created,
                LoginDate = user.LoginDate,
                Name = user.Name,
                Age = user.Age,
                Gender = user.Gender.ToString("G"),
                IsMarried = user.IsMarried.ToString("G"),
                Description = user.Description,
                Token = token,
                Roles = user.RoleUsers.Select(x => x.Role.RoleName).ToList(),
            };

            if (user.Country != null)
            {
                userView.Country = new CountryView
                {
                    Id = user.Country.Id,
                    Name = user.Country.Name,
                    Description = user.Country.Description,
                    Code = user.Country.Code
                };
            }

            if (user.Subscriptions != null && user.Subscriptions.Any())
            {
                userView.Subscriptions = user.Subscriptions.Select(x => new UserSub
                {
                    Id = x.UserId,
                });
            }

            if (user.Subscribers != null && user.Subscribers.Any())
            {
                userView.Subscribers = user.Subscribers.Select(x => new UserSub
                {
                    Id = x.SubscriberId,
                });
            }

            if (user.Files != null && user.Files.Any(x => x.Type == FileType.UserImage))
            {
                userView.UserImage = user.Files.FirstOrDefault(x => x.Type == FileType.UserImage).Path;
            }

            if (user.Products != null && user.Products.Any())
            {
                userView.Products = user.Products.Count;
            }

            return userView;
        }
    }
}
