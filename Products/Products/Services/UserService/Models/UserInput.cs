﻿using Products.Models;

namespace Products.Services
{
    public class UserInput
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
}