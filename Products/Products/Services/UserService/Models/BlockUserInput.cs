﻿namespace Products.Services
{
    public class BlockUserInput
    {
        public int UserId { get; set; }
        public bool? IsBlocked { get; set; }
    }
}