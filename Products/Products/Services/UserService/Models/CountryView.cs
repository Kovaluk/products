﻿using Products.Models;

namespace Products.Services
{
    public class CountryView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }

    public static class CountryViewExtension
    {
        public static CountryView ToView(this Country country)
        {
            return new CountryView
            {
                Id = country.Id,
                Name = country.Name,
                Description = country.Description,
                Code = country.Code,
            };
        }
    }
}