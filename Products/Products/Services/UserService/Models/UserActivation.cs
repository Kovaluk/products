﻿namespace Products.Services
{
    public class UserActivation
    {
        public string Email { get; set; }
        public string Code { get; set; }
    }
}
