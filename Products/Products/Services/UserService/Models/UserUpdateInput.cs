﻿using Products.Models;

namespace Products.Services
{
    public class UserUpdateInput
    {
        public int? Age { get; set; }
        public Gender? Gender { get; set; }
        public Married? IsMarried { get; set; }
        public string Description { get; set; }
        public int? CountryId { get; set; }
    }
}