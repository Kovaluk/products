﻿using System;
using Products.Models;
using System.Linq;

namespace Products.Services
{
    public class UserPublicView
    {
        public string Name { get; set; }
        public int? Age { get; set; }
        public string Gender { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public CountryView Country { get; set; }
        public int Subscribers { get; set; }
        public int Products { get; set; }
        public bool IsCurrentUserSubscribed { get; set; }
        public bool IsCurrentUserProfile { get; set; }
        public string UserImage { get; set; }
    }

    public static class UserPublicViewExtension
    {
        public static UserPublicView ToPublicView(this User user, int contextUserId)
        {
            var userPublicView = new UserPublicView
            {
                Name = user.Name,
                Age = user.Age,
                Gender = user.Gender.ToString("G"),
                Description = user.Description,
                Created = user.Created,
                IsCurrentUserProfile = user.Id == contextUserId,
            };

            if (user.Country != null)
            {
                userPublicView.Country = new CountryView
                {
                    Id = user.Country.Id,
                    Name = user.Country.Name,
                    Description = user.Country.Description,
                    Code = user.Country.Code
                };
            }

            if (user.Subscribers != null && user.Subscribers.Any())
            {
                userPublicView.Subscribers = user.Subscribers.Count;

                userPublicView.IsCurrentUserSubscribed = user.Subscribers.Any(x => x.SubscriberId == contextUserId);
            }

            if (user.Products != null && user.Products.Any())
            {
                var productsPublic = user.Products.Where(x => x.Status == ProductStatus.Approved).ToList();

                if (productsPublic.Any())
                {
                    userPublicView.Products = productsPublic.Count;
                }
            }

            if (user.Files != null && user.Files.Any(x => x.Type == FileType.UserImage))
            {
                userPublicView.UserImage = user.Files.FirstOrDefault(x => x.Type == FileType.UserImage).Path;
            }

            return userPublicView;
        }
    }
}
