﻿using System;
using Products.Models;
using System.Linq;

namespace Products.Services
{
    public class UserAdminView
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime? LoginDate { get; set; }
        public bool IsBlocked { get; set; }
        public string UserImage { get; set; }
    }

    public static class UserAdminViewExtension
    {
        public static UserAdminView ToAdminView(this User user)
        {
            var userAdminView = new UserAdminView
            {
                Id = user.Id,
                Email = user.Email,
                Name = user.Name,
                Created = user.Created,
                LoginDate = user.LoginDate,
                IsBlocked = user.IsBlocked,
            };

            if (user.Files != null && user.Files.Any(x => x.Type == FileType.UserImage))
            {
                userAdminView.UserImage = user.Files.FirstOrDefault(x => x.Type == FileType.UserImage).Path;
            }

            return userAdminView;
        }
    }
}
