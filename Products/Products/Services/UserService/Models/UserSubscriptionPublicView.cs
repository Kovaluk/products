﻿using System;
using Products.Models;
using System.Linq;

namespace Products.Services
{
    public class UserSubscriptionPublicView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? Age { get; set; }
        public string Gender { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public CountryView Country { get; set; }
        public int Products { get; set; }
        public DateTime? LastUpdate { get; set; }
    }

    public static class UserSubscriptionPublicViewExtension
    {
        public static UserSubscriptionPublicView ToUserSubscriptionPublicViewView(this User user)
        {
            var userSubscriptionPublicView = new UserSubscriptionPublicView
            {
                Id = user.Id,
                Name = user.Name,
                Age = user.Age,
                Gender = user.Gender.ToString("G"),
                Description = user.Description,
                Created = user.Created,
            };

            if (user.Country != null)
            {
                userSubscriptionPublicView.Country = new CountryView
                {
                    Id = user.Country.Id,
                    Name = user.Country.Name,
                    Description = user.Country.Description,
                    Code = user.Country.Code
                };
            }

            if (user.Products != null && user.Products.Any())
            {
                var productsPublic = user.Products.Where(x => x.Status == ProductStatus.Approved).ToList();

                if (productsPublic.Any())
                {
                    userSubscriptionPublicView.Products = productsPublic.Count;
                    userSubscriptionPublicView.LastUpdate = productsPublic.OrderByDescending(x => x.Created).FirstOrDefault().Created;
                }
            }

            return userSubscriptionPublicView;
        }
    }
}
