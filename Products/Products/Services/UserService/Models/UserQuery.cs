﻿namespace Products.Services
{
    public class UserQuery
    {
        public int? UserId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public bool? IsBlocked { get; set; }
    }
}
