﻿using Products.Models;

namespace Products.Services
{
    public interface IJWTService
    {
        string GetToken(User user);
    }
}
