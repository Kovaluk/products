﻿namespace Products.Services
{
    public interface ILogService
    {
        void Log(string log);
        void LogException(string log);
    }
}
