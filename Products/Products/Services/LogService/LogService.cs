﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Products.Services
{
    public class LogService : ILogService
    {
        private readonly object locker = new object();
        private readonly IWebHostEnvironment _webHostEnvironment;

        private readonly string _contentRootPath;

        public LogService(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
            _contentRootPath = _webHostEnvironment.ContentRootPath;
        }

        public void Log(string log)
        {
            lock (locker)
            {
                var path = Path.Combine(_contentRootPath, "logs.txt");

                using (StreamWriter sw = new StreamWriter(path, true))
                {
                    sw.WriteLine("Log Message");
                    sw.WriteLine($"Log time: [{DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss")}]");
                    sw.WriteLine("Message:");
                    sw.WriteLine(log);
                    sw.WriteLine(string.Empty);
                    sw.WriteLine("------------------------------");
                    sw.WriteLine(string.Empty);
                }
            }
        }

        public void LogException(string log)
        {
            lock (locker)
            {
                var path = Path.Combine(_contentRootPath, "logsErrors.txt");

                using (StreamWriter sw = new StreamWriter(path, true))
                {
                    sw.WriteLine("Error Log Message");
                    sw.WriteLine($"Log time: [{DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss")}]");
                    sw.WriteLine("Message:");
                    sw.WriteLine(log);
                    sw.WriteLine(string.Empty);
                    sw.WriteLine("------------------------------");
                    sw.WriteLine(string.Empty);
                }
            }
        }
    }
}