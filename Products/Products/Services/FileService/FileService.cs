﻿using System;
using System.Threading.Tasks;
using Products.Models;
using Microsoft.AspNetCore.Http;
using DIR = System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Collections.Generic;

namespace Products.Services
{
    public class FileService : IFileService
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        private readonly string _contentRootPath;

        private const string Slash = "/";
        private const string StaticRootFolder = "/wwwroot";
        private const string ImageSizeTypeTemplate = "{size}";
        private const string ImageSizeTypeTemplateFull = "{size}-";
        private const long ImageQualityStandart = 75;

        private readonly Dictionary<FileType, string> FileTypePathMap = new Dictionary<FileType, string>
        {
            { FileType.UserImage, "/images/users" },
            { FileType.ProductImage, "/images/products" },
        };

        private readonly Dictionary<string, ImageFormat> ImageFormatMap = new Dictionary<string, ImageFormat>
        {
            { ".png", ImageFormat.Png },
            { ".jpg", ImageFormat.Jpeg },
            { ".jpeg", ImageFormat.Jpeg },
        };

        private readonly Dictionary<ImageSizeType, int> ImageSizeTypeMap = new Dictionary<ImageSizeType, int>
        {
            { ImageSizeType.sm, 600 },
            { ImageSizeType.md, 900 },
            { ImageSizeType.lg, 1200 },
        };

        private readonly Dictionary<FileType, string[]> FileTypeFormatsMap = new Dictionary<FileType, string[]>
        {
            { FileType.UserImage, new string[] { "png", "jpg", "jpeg" } },
            { FileType.ProductImage, new string[] { "png", "jpg", "jpeg" } },
        };

        public FileService(
            IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
            _contentRootPath = _webHostEnvironment.ContentRootPath;
        }

        public void AddFile(IFormFile formFile, File file)
        {
            var baseFolder = _contentRootPath + StaticRootFolder;
            var fileFolder = FileTypePathMap[file.Type];
            var directory = baseFolder + fileFolder;

            CheckDirectory(directory);

            if (!string.IsNullOrEmpty(file.Path))
            {
                RemoveFile(file.Path);
            }

            string guid = Guid.NewGuid().ToString();
            string fileType = file.Name.Substring(file.Name.LastIndexOf("."));

            file.Guid = guid;

            var path = directory + Slash + ImageSizeTypeTemplateFull + guid + fileType;

            var imageFormat = ImageFormatMap[fileType];

            foreach (var imageSize in ImageSizeTypeMap)
            {
                SaveFile(formFile, path, imageFormat, imageSize.Key.ToString("G"), imageSize.Value);
            }

            file.Path = path.Replace(baseFolder, string.Empty);
        }

        public void RemoveFile(string filePath)
        {
            foreach (var imageSize in ImageSizeTypeMap)
            {
                var filePathWithSize = filePath.Replace(ImageSizeTypeTemplate, imageSize.Key.ToString("G"));
                var path = _contentRootPath + Slash + StaticRootFolder + filePathWithSize;

                if (DIR.File.Exists(path))
                {
                    DIR.File.Delete(path);
                }
            }
        }

        public bool IsFileTypeValid(IFormFile formFile, FileType fileType)
        {
            var fileFormat = formFile.ContentType;
            var formats = FileTypeFormatsMap[fileType];
            return formats.Any(x => fileFormat.Contains(x));
        }

        private void CheckDirectory(string path)
        {
            if (!DIR.Directory.Exists(path))
            {
                DIR.Directory.CreateDirectory(path);
            }
        }

        private void SaveFile(IFormFile formFile, string path, ImageFormat imageFormat, string sizeType, int size)
        {
            using (var image = Image.FromStream(formFile.OpenReadStream()))
            {
                int width, height;

                if (image.Width < size && image.Height < size)
                {
                    width = image.Width;
                    height = image.Height;
                }
                else if (image.Width > image.Height)
                {
                    width = size;
                    height = Convert.ToInt32(image.Height * size / (double)image.Width);
                }
                else
                {
                    width = Convert.ToInt32(image.Width * size / (double)image.Height);
                    height = size;
                }

                var resized = new Bitmap(width, height);

                using (var graphics = Graphics.FromImage(resized))
                {
                    graphics.CompositingQuality = CompositingQuality.HighSpeed;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.DrawImage(image, 0, 0, width, height);

                    using (var output = DIR.File.Open(path.Replace(ImageSizeTypeTemplate, sizeType), DIR.FileMode.Create))
                    {
                        var qualityParamId = Encoder.Quality;
                        var encoderParameters = new EncoderParameters(1);
                        encoderParameters.Param[0] = new EncoderParameter(qualityParamId, ImageQualityStandart);
                        var codec = ImageCodecInfo.GetImageDecoders().FirstOrDefault(x => x.FormatID == imageFormat.Guid);
                        resized.Save(output, codec, encoderParameters);
                    }
                }
            }
        }
    }
}
