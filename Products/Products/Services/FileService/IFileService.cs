﻿using Products.Models;
using Microsoft.AspNetCore.Http;

namespace Products.Services
{
    public interface IFileService
    {
        void AddFile(IFormFile formFile, File file);
        void RemoveFile(string filePath);
        bool IsFileTypeValid(IFormFile formFile, FileType fileType);
    }
}
