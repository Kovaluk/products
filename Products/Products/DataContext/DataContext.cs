﻿using Microsoft.EntityFrameworkCore;
using Products.Models;

namespace Products.DataContext
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options)
            : base(options)
        { }

        public DbSet<Country> Countries { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<SecurityCode> SecurityCodes { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ProductLike> ProductLikes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>().ToTable(nameof(Countries));

            modelBuilder.Entity<File>()
                .HasOne(x => x.User)
                .WithMany(x => x.Files)
                .HasForeignKey(x => x.UserId)
                .IsRequired();

            modelBuilder.Entity<File>()
                .HasOne(x => x.Product)
                .WithMany(x => x.Files)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<File>().ToTable(nameof(Files));

            modelBuilder.Entity<Product>()
                .HasOne(c => c.User)
                .WithMany(u => u.Products)
                .HasForeignKey(u => u.UserId)
                .IsRequired();
            modelBuilder.Entity<Product>().ToTable(nameof(Products));

            modelBuilder.Entity<ProductType>()
                .HasMany(p => p.Products)
                .WithOne(pt => pt.ProductType)
                .HasForeignKey(pt => pt.ProductTypeId)
                .IsRequired();
            modelBuilder.Entity<ProductType>().ToTable(nameof(ProductTypes));

            modelBuilder.Entity<RoleUser>()
                .HasKey(ru => new { ru.RoleId, ru.UserId });
            modelBuilder.Entity<RoleUser>()
                .HasOne(ru => ru.Role)
                .WithMany(r => r.RoleUsers)
                .HasForeignKey(ru => ru.RoleId);
            modelBuilder.Entity<RoleUser>()
                .HasOne(ru => ru.User)
                .WithMany(u => u.RoleUsers)
                .HasForeignKey(ru => ru.UserId);
            modelBuilder.Entity<Role>().ToTable(nameof(Roles));

            modelBuilder.Entity<SecurityCode>()
                .HasOne(s => s.User)
                .WithMany(u => u.SecurityCodes)
                .HasForeignKey(u =>  u.UserId)
                .IsRequired();
            modelBuilder.Entity<SecurityCode>().ToTable(nameof(SecurityCodes));

            modelBuilder.Entity<Subscription>()
                .HasKey(su => new { su.SubscriberId, su.UserId });
            modelBuilder.Entity<Subscription>()
                .HasOne(su => su.Subscriber)
                .WithMany(s => s.Subscriptions)
                .HasForeignKey(su => su.SubscriberId);
            modelBuilder.Entity<Subscription>()
                .HasOne(su => su.User)
                .WithMany(s => s.Subscribers)
                .HasForeignKey(su => su.UserId);

            modelBuilder.Entity<User>()
                .HasOne(x => x.Country)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.CountryId);
            modelBuilder.Entity<User>().ToTable(nameof(Users));

            modelBuilder.Entity<ProductLike>()
                .HasKey(li => new { li.UserId, li.ProductId });

            modelBuilder.Entity<User>()
                .HasMany(u => u.ProductLikes)
                .WithOne(l => l.User)
                .HasForeignKey(l => l.UserId);

            modelBuilder.Entity<Product>()
                .HasMany(p => p.ProductLikes)
                .WithOne(l => l.Product)
                .HasForeignKey(l => l.ProductId)
                .OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(modelBuilder);
        }
    }
}
