﻿namespace Products.ModelsCommon
{
    public class BaseQuery
    {
        public int? Skip { get; set; }
        public int? Take { get; set; }
        public bool IsAscending { get; set; }
    }
}
