﻿namespace Products.Models
{
    public enum SecurityCodeType
    {
        Registration = 1,
        PasswordChange = 2,
    }
}