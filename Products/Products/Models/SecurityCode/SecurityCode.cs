﻿using System;

namespace Products.Models
{
    public class SecurityCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public SecurityCodeType Type { get; set; }
        public DateTime Created { get; set; }
        public DateTime Expired { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}