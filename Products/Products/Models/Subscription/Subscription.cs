﻿
namespace Products.Models
{
    public class Subscription
    {
        public int SubscriberId { get; set; }
        public User Subscriber { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
