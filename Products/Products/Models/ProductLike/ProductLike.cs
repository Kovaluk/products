﻿
namespace Products.Models
{
    public class ProductLike
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public bool IsLike { get; set; }
    }
}
