﻿using System;
using System.Collections.Generic;

namespace Products.Models
{
    public class User
    {
        public User()
        {
            RoleUsers = new HashSet<RoleUser>();
            SecurityCodes = new HashSet<SecurityCode>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime Created { get; set; }
        public DateTime? LoginDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsBlocked { get; set; }
        public string Name { get; set; }
        public int? Age { get; set; }
        public Gender Gender { get; set; }
        public Married IsMarried { get; set; }
        public string Description { get; set; }

        public int? CountryId { get; set; }
        public Country Country { get; set; }

        public ICollection<RoleUser> RoleUsers { get; set; }
        public ICollection<SecurityCode> SecurityCodes { get; set; }
        public ICollection<Subscription> Subscriptions { get; set; }
        public ICollection<Subscription> Subscribers { get; set; }
        public ICollection<Product> Products { get; set; }
        public ICollection<File> Files { get; set; }
        public ICollection<ProductLike> ProductLikes { get; set; }
    }
}