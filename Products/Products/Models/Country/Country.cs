﻿using Products.Models;
using System.Collections.Generic;

namespace Products.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }

        public ICollection<User> Users { get; set; }
    }
}