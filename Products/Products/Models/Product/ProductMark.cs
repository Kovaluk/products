﻿namespace Products.Models
{
    public enum ProductMark
    {
        Bad = 0,
        Normal = 1,
        Good = 2,
        Perfect = 3,
    }
}
