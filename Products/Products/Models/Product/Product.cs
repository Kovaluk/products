﻿using System;
using System.Collections.Generic;

namespace Products.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ProductMark Mark { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public ProductStatus Status { get; set; }
        public string AdminComment { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
        public int ProductTypeId { get; set; }
        public ProductType ProductType { get; set; }

        public ICollection<File> Files { get; set; }
        public ICollection<ProductLike> ProductLikes { get; set; }
    }
}
