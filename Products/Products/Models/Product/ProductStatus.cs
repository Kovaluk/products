﻿namespace Products.Models
{
    public enum ProductStatus
    {
        Pending = 0,
        Rejected = 1,
        Approved = 2,
    }
}
