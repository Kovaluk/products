﻿using System;

namespace Products.Models
{
    public class File
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Guid { get; set; }
        public string Path { get; set; }
        public DateTime Created { get; set; }
        public FileType Type { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int? ProductId { get; set; }
        public Product Product { get; set; }
    }
}
