﻿namespace Products.Models
{
    public enum ImageSizeType
    {
        sm = 0,
        md = 1,
        lg = 2,
    }
}
