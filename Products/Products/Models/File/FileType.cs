﻿namespace Products.Models
{
    public enum FileType
    {
        UserImage = 0,
        ProductImage = 1,
    }
}
