﻿using System.Collections.Generic;

namespace Products.Models
{
    public class Role
    {
        public Role()
        {
            RoleUsers = new HashSet<RoleUser>();
        }

        public Role(RoleType roleEnum)
        {
            RoleType = roleEnum;
            RoleName = roleEnum.ToString("G");
        }
        
        public int Id { get; set; }
        public RoleType RoleType { get; set; }
        public string RoleName { get; set; }

        public ICollection<RoleUser> RoleUsers { get; set; }
    }
}