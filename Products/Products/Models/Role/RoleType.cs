﻿namespace Products.Models
{
    public enum RoleType
    {
        Admin = 1,
        User = 2,
    }
}